package com.manoj.movies.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;



public class Video {

    @SerializedName("results")
    private
    ArrayList<Trailer> trailers;

    public ArrayList<Trailer> getTrailers() {
        return trailers;
    }

    public void setTrailers(ArrayList<Trailer> trailers) {
        this.trailers = trailers;
    }
}
