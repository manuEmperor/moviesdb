package com.manoj.movies.models;

import com.google.gson.annotations.SerializedName;


public class BackdropImage {

    @SerializedName("file_path")
    private String bannerImageLink;

    public String getBannerImageLink() {
        return bannerImageLink;
    }

    public void setBannerImageLink(String bannerImageLink) {
        this.bannerImageLink = bannerImageLink;
    }
}
