package com.manoj.movies.network;


public class URLConstants {

    public static final String MOVIE_BASE_URL = "https://api.themoviedb.org/3/movie/";
    public static final String SEARCH_BASE_URL = "https://api.themoviedb.org/3/search/";
    public static final String API_KEY = "f844df643b1485cb6ca5cc9ce562eba1";
    public static final String IMAGE_BASE_URL = "http://image.tmdb.org/t/p/w300/";
    public static final String BANNER_BASE_URL = "http://image.tmdb.org/t/p/w500/";
    public static final String TRAILER_THUMBNAIL_IMAGE_URL = "http://img.youtube.com/vi/";
    public static final String YOUTUBE_VIDEO_PLAYER_API_KEY = "AIzaSyAmyORazLl7Y6DE8wZeZ_Ub3ykP7MM5J1Q";
}
