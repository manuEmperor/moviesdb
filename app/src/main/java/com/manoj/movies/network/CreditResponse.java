package com.manoj.movies.network;

import com.manoj.movies.models.Cast;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;



public class CreditResponse {

    @SerializedName("cast")
    private ArrayList<Cast> cast;

    public ArrayList<Cast> getCast() {
        return cast;
    }

    public void setMovieCast(ArrayList<Cast> cast) {
        this.cast = cast;
    }

}
