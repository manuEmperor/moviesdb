package com.manoj.movies.network;

import com.manoj.movies.models.Review;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class ReviewResponse {

    @SerializedName("results")
    private ArrayList<Review> reviews;

    public ArrayList<Review> getReviews() {
        return reviews;
    }

    public void setReviews(ArrayList<Review> reviews) {
        this.reviews = reviews;
    }
}
