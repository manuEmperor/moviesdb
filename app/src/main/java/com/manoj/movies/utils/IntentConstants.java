package com.manoj.movies.utils;


public class IntentConstants {

    public static final String INTENT_KEY_MOVIE_ID = "movie_id";
    public static final String INTENT_KEY_POSTER_PATH = "posterPath";
    public static final String INTENT_KEY_MOVIE_NAME = "movieName";
    public static final String INTENT_KEY_TVSHOW_ID = "tvShow_id";
    public static final String INTENT_KEY_TVSHOW_NAME = "tvShowName";

}
