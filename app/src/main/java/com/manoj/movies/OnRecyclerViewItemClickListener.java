package com.manoj.movies;

import android.view.View;


public interface OnRecyclerViewItemClickListener {

    void onRecyclerViewItemClicked(int verticalPosition, int horizontalPosition, View view);
}
